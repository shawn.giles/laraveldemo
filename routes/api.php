<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('/v1/user-profile','Api\V1\UserController@create')->name('post-user-profile');
Route::get('/v1/user-profile','Api\V1\UserController@showall')->name('get-user-data');
Route::get('/v1/user-profile/{id}','Api\V1\UserController@showbyid')->name('get-user-data-by-id');
Route::put('/v1/user-profile-update/{id}','Api\V1\UserController@updatebyid')->name('post-update-user-profile');
Route::delete('/v1/user-profile-delete/{id}','Api\V1\UserController@deletebyid')->name('post-update-user-profile');



