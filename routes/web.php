<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
	//Login
	Route::get('/login','LoginController@index')->name('login');	
	Route::post('/login/loginData','LoginController@authenticate')->name('loginData');

	//Register
	Route::get('/register','RegisterController@index')->name('register');	
	Route::post('/register/registerData','RegisterController@store')->name('registerData');

Route::group(['prefix' => '', 'middleware' => ['admin']], function () {
	//dashbord
	Route::get('/','DashboardController@index')->name('dashboard');

	//category
	Route::get('/category/list','CategoryController@index')->name('catagorylist');
	Route::get('/category/add','CategoryController@create')->name('catagoryadd');
	Route::post('/category/catagoryData','CategoryController@store')->name('catagoryData');
	Route::get('/category/catagoryDelete/{id}','CategoryController@delete')->name('deleteCatagory');
	Route::get('/category/catagoryEdit/{id}','CategoryController@edit')->name('catagoryEdit');

	//product
	Route::get('/product/list','ProductController@index')->name('productlist');
	Route::get('/product/add','ProductController@create')->name('productadd');
	Route::post('/product/productData','ProductController@store')->name('productData');
	Route::get('/product/productDelete/{id}','ProductController@delete')->name('deleteProduct');
	Route::get('/product/productEdit/{id}','ProductController@edit')->name('productEdit');
});

Route::get('/logout','LoginController@logout')->name('logout');	

	// Route::get('/register', function () {
	// 	return view('admin.auth.register');
 	// });

	// Route::resource('photos', 'PhotoController')->except([
	// 	'index','create','edit'
	// ]);
		// Route::resource('photos.comments', 'PhotoController');
		// Route::get('add-photos','PhotoController@addPhotos');
		// Route::get('update-photos','PhotoController@updatePhotos');
		// Route::get('delete-photos','PhotoController@deletePhotos');


		// Route::get('categorys','PhotoController@index');
		
		// Route::post('/contact','ContactController@store')->name('contactstore');

	//  Route::get('/categorys', function () {
	//      return view('category_details');
		// })->middleware('test');
		
		// Route::get('/categorys/name/details', function () {											// named routing
	//        return view('category_details');
	//  	})->name('catagory_details');

	// Route::get('/user/{name}', function ($name) {												//single require parametar
	//       return "welcome " . $name;
	// });

	// Route::get('/user/{name?}', function ($name="None") {										//single optional parametar
	//        return "welcome " . $name;
	// });

	// Route::get('/user/{name}', function ($name) {												//single conditional parametar //       return "welcome " . $name;
	// })->where('name','[a-zA-Z]+');
	
	// Route::get('/user/{name}/{age}', function ($name,$age) {										//multiple require parametar
	//       return " welcome your name is " . $name . " And your age is " . $age;
	// });

	// Route::get('/user/{name?}/{age?}', function ($name="None",$age="None") {						//multiple optional parametar
	//        return " welcome your name is " . $name . " And your age is " . $age;
	// });

	// Route::get('/user/{name}/{age}', function ($name,$age) {										//multiple conditional parametar
	//       return " welcome your name is " . $name . " And your age is " . $age;
	// })->where(['name'=>'[a-zA-Z]+','age'=>'[0-9]+']);

	// 	Route::get('/categorys', function () {														// print url
	//  		$URL = route('catagory_details');
	//  		return $URL;
	// 	});

		// Route::get('/categorys', function () {												// direct redirect in catagory_details
	//         return redirect()->route('catagory_details');
	//  	});
		

	//Route::get('/product', 'ProductController@index');

	//Route::view('/','welcome');

	//Route::view('/welcome', 'welcome');
