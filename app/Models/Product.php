<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\ProductImages;
class Product extends Model
{
    protected $table = 'product';

    public $timestamps = false;

    protected $fillable = [
		'id ',
		'p_name',
		'product_code',
		'p_cname',
        'p_price',
        'p_sale_price',
		'p_quantity',
		'p_orders',
		'p_status',
		'p_added_date',
		'p_updated_date',
	];

	function pro(){
        return $this->hasOne(ProductImages::class,'product_id','id');
    }
}
