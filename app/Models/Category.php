<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $table = 'categorys';

    public $timestamps = false;

    protected $fillable = [
		'id ',
		'c_name',
		'c_images',
		'c_orders',
		'c_status',
		'c_added_date',
		'c_updated_date',
	];
}
