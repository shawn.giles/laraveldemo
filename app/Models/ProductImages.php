<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProductImages extends Model
{
    protected $table = 'product_images';

    public $timestamps = false;

    protected $fillable = [
		'id ',
		'product_id',
		'image_name'
	];
}
