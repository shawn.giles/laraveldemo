<?php

namespace App\Models;

use Illuminate\Foundation\Auth\User as Authenticatable;

class Register extends Authenticatable
{
    protected $table = 'register';

    public $timestamps = false;

    protected $fillable = [
		'id ',
		'name',
		'email',
		'password'
	];
}
