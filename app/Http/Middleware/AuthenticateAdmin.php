<?php

namespace App\Http\Middleware;

use Closure, Route;
use Illuminate\Support\Facades\Auth;
// use Illuminate\Contracts\Auth\Guard;

class AuthenticateAdmin
{
    /*protected $auth;

    public function __construct( Guard $auth ){
        $this->auth= $auth;
    }*/

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = 'admin'){
        if (Auth::guard($guard)->guest()) {
            return redirect(route('login'));
        }
        return $next($request);
    }
}
