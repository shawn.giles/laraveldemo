<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Category;
use App\Models\Product;
use App\Models\ProductImages;
use App\Models\ProductCategories;

use Illuminate\Support\Facades\DB;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {   
        //  $product = Product::all();
         //$productimages = ProductImages::all()->toArray();
        $product = DB::table('product')
        ->leftjoin('product_images','product_images.product_id','=','product.id')
        ->leftjoin('product_categories','product_categories.product_id','=','product.id')
        ->leftjoin('categorys','categorys.id','=','product_categories.category_id')
        ->select('product.id',
        'product.p_name',
        'product.product_code',
        'product.p_price',
        'product.p_sale_price',
        'product.p_quantity',
        'product.p_orders',
        'product.p_status',
        'product.p_added_date',
        'product.p_updated_date',
        'product_images.id as Pids',
        'product_images.image_name as Pimgs',
        DB::raw('group_concat(DISTINCT categorys.id) as catIDS'),
        DB::raw('group_concat(DISTINCT categorys.c_name) as CatNAMES'))
        ->groupBy('product.id')
        ->get();
        //print_r($product);exit;
        return view('admin.product.index', compact('product'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {   
        //$categorys = DB::table("categorys")->where('c_status',1)->get()->toArray();
        $categorys = Category::all()->where('c_status', 1)->toArray();
        //print_r($categorys);exit;
        return view('admin.product.form', compact('categorys'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {   
        $request->validate([
            'p_name' => 'required',
            'p_price' => 'required',
            'p_sale_price' => 'required',
            'p_quantity' => 'required',
            'p_orders' => 'required',
            'p_status' => 'required'
        ]);
		//dd($editProduct);
        $data = $request->all();
        if ($request->isMethod('post')) {
            $product_codes = "PRODUCT_".rand(100000,990000);
            $data = $request->all();
            $added_date = time();
            $updated_date = time();
            
            $product = ($data['id']) ? Product::find($data['id']) : new Product;
            // print_r($data['id']); exit;
            $product->p_name = $data['p_name'];           
            $product->p_price   = $data['p_price'];
            $product->p_sale_price = $data['p_sale_price'];
            $product->p_quantity = $data['p_quantity'];
            $product->p_orders = $data['p_orders'];
            $product->p_status   = $data['p_status'];
            if(!empty($data['id'])){
                $product->p_updated_date = $updated_date;
            }else{
                $product->p_added_date = $added_date;
                $product->product_code = $product_codes;

            }
            $product->save();

            $subimage = $request->file('p_images');

            $insertedId = $product->id;
            
            if(!empty($data['id'])){
                if(!empty($subimage)){
                    DB::table("product_images")->where('product_id',$data['id'])->delete();
                    foreach($subimage as $key => $pic){
                        $new_names = $pic->getClientOriginalName();
                        $efile   = $pic->store('product_images','public');
                        $productimages = new ProductImages;
                        $productimages->product_id = $data['id'];
                        $productimages->image_name = $efile;
                        $productimages->save();
                    }
                }                
            }else{
                foreach($subimage as $key => $pic){
                    $new_names = $pic->getClientOriginalName();
                    $efile   = $pic->store('product_images','public');
                    $productimages = new ProductImages;
                    $productimages->product_id = $insertedId;
                    $productimages->image_name = $efile;
                    $productimages->save();
                }
            }

            $categorys = $data['category'];

            if(!empty($data['id'])){
                DB::table("product_categories")->where('product_id',$data['id'])->delete();
                foreach($categorys as $key => $catids){
                    $productcategories = new ProductCategories;
                    $productcategories->product_id = $data['id'];
                    $productcategories->category_id = $catids;
                    $productcategories->save();
                }
            }else{
                foreach($categorys as $key => $catids){
                    $productcategories = new ProductCategories;
                    $productcategories->product_id = $insertedId;
                    $productcategories->category_id = $catids;
                    $productcategories->save();
                }
            }
            if(!empty($data['id'])){
                return redirect('/product/list')->with('success','Product Sucessfully Edited');
            }
            else{
                return redirect('/product/list')->with('success','Product Sucessfully Inserted');
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {   
        $categorys = Category::all()->toArray();
        // echo "edit called ".$id ;
        // $editProduct = Product::find($id);
        // dd($editcategory);

        $editProducts = DB::table('product')
        ->leftjoin('product_images','product_images.product_id','=','product.id')
        ->leftjoin('product_categories','product_categories.product_id','=','product.id')
        ->leftjoin('categorys','categorys.id','=','product_categories.category_id')
        ->select('product.*',
        DB::raw('group_concat(DISTINCT product_images.id) as PimgIDS'),
        DB::raw('group_concat(DISTINCT product_images.image_name) as PimgNAMES'),
        DB::raw('group_concat(DISTINCT categorys.id) as catIDS'),
        DB::raw('group_concat(DISTINCT categorys.c_name) as CatNAMES'))
        ->where('product.id', $id)
        ->groupBy('product.id')
        ->get();
        // print_r($editProducts); exit;
        $editProduct = json_decode(json_encode($editProducts), true);

        //print_r($editProduct);exit;
        return view('admin.product.form', compact('editProduct', 'id' ,'categorys') );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete($id)
    {
        // $productimages = ProductImages::all()->toArray();

        // $product = DB::table('product')
        // ->leftjoin('product_images','product_images.product_id','=','product.id')
        // ->select('product.id',
        // 'product.p_name',
        // 'product.product_code',
        // 'product.p_cname',
        // 'product.p_price',
        // 'product.p_sale_price',
        // 'product.p_quantity',
        // 'product.p_orders',
        // 'product.p_status',
        // 'product.p_added_date',
        // 'product.p_updated_date',
        // DB::raw('group_concat(DISTINCT product_images.id) as Pids'),
        // DB::raw('group_concat(DISTINCT product_images.image_name) as Pimgs'))
        // ->where('product.id', $id)
        // ->groupBy('product.id')
        // ->get();

        // $value = json_decode(json_encode($product), true);
        
        // //print_r($value[0]['Pids']);exit;

        // $explode = explode(",", $value[0]['Pids']);
        
        //print_r($explode);exit;

        echo "delete called".$id ;
        $deleteid =  Product::findOrFail($id);
        //print_r($deleteid);exit;
        $deleteid->delete(); 
        //echo "<pre>";
        //print_r( DB::table("product_images")->whereIn('product_id',$explode));exit;

        //print_r(DB::table("product")->delete($id));exit;

        //DB::table("product")->delete($id);
        DB::table("product_images")->where('product_id',$id)->delete();
        DB::table("product_categories")->where('product_id',$id)->delete();

       // $productimages = ProductImages::whereIn('product_id',$value[0]['Pids']);

        return redirect('/product/list')->with('success-delete','Product Sucessfully deleted');
    }
}
