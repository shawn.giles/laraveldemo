<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Category;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {   
        $category = Category::all()->toArray();
        //print_r($category);exit();
        return view('admin.category.index', compact('category'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.category.form');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {   
       
        if(isset($request->id)){
            $request->validate([
                'c_name' => 'required',
                'c_orders' => 'required',
                'c_status' => 'required'
            ]);
            $cname = trim(strtolower(request('c_name')));
            if(Category::where('c_name', '=', $cname)->where('id', '!=', $request->id)->count() ){
                return redirect()->back()->with('alert-info', 'Name Is Already Exists.');
            }
            else{   
                if ($request->isMethod('post')) {
                    $data = $request->all();
                    $added_date = time();
                    $updated_date = time();
    
                    //File upload code
                    // $request->validate([
                    //     'c_images' => 'required|mimes:jpeg,png,jpg',
                    // ]);
                    $category = ($data['id']) ? Category::find($data['id']) : new Category;
    
                    if(empty($request->c_images)) {
                        $category->c_images = $request->old_imgs;
                    } else {
                            $file = $request->file('c_images')->store('category_images','public');
                            $exploade = explode("/",$file);
                            $category->c_images = $exploade[1];  
                    }
                    $category->c_name   = $data['c_name'];
                    $category->c_orders = $data['c_orders'];
                    $category->c_status = $data['c_status'];
                    if(isset($data['id']))
                    {
                        $category->c_updated_date = $updated_date;
                    }else{
                        $category->c_added_date = $added_date;
                        $category->c_updated_date = $updated_date;
                    }
                    $category->save();
    
                    if(isset($data['id']))
                    {
                        return redirect('/category/list')->with('success','Category Sucessfully Edited');
                    }else{
                        return redirect('/category/list')->with('success','Category Sucessfully Inserted');
                    }
    
                }
            }    
        }
        else{
            $request->validate([
                'c_name' => 'required',
                'c_images' => 'required',
                'c_orders' => 'required',
                'c_status' => 'required'
            ]);
            $cname = trim(strtolower(request('c_name')));
            if(Category::where('c_name', '=', $cname)->count()){
                return redirect()->back()->with('alert-info', 'Name Is Already Exists.');
            }
            else{   
                if ($request->isMethod('post')) {
                    $data = $request->all();
                    $added_date = time();
                    $updated_date = time();
    
                    //File upload code
                    // $request->validate([
                    //     'c_images' => 'required|mimes:jpeg,png,jpg',
                    // ]);
                    $category = ($data['id']) ? Category::find($data['id']) : new Category;
    
                    if(empty($request->c_images)) {
                        $category->c_images = $request->old_imgs;
                    } else {
                            $file = $request->file('c_images')->store('category_images','public');
                            $exploade = explode("/",$file);
                            $category->c_images = $exploade[1];  
                    }
                    $category->c_name   = $data['c_name'];
                    $category->c_orders = $data['c_orders'];
                    $category->c_status = $data['c_status'];
                    if(isset($data['id']))
                    {
                        $category->c_updated_date = $updated_date;
                    }else{
                        $category->c_added_date = $added_date;
                        $category->c_updated_date = $updated_date;
                    }
                    $category->save();
    
                    if(isset($data['id']))
                    {
                        return redirect('/category/list')->with('success','Category Sucessfully Edited');
                    }else{
                        return redirect('/category/list')->with('success','Category Sucessfully Inserted');
                    }
    
                }
            }  
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $editcategory = Category::find($id);
        // dd($editcategory);
        
        return view('admin.category.form', compact('editcategory', 'id') );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete($id)
    {
        echo "delete called".$id ;
        $deleteid =  Category::findOrFail($id);
        $deleteid->delete(); 

        return redirect('/category/list')->with('success-delete','Category Sucessfully deleted');
    }
}
