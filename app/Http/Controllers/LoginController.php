<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Register;
use Hash;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.auth.login');
    }

    // public function store(Request $request)
    // {
    //     if ($request->isMethod('post')) {
    //         $data = $request->all();

    //         $email = $data['email'];
    //         $password = $data['password'];
       
    //         $user = Register::where('email', '=', $email)->first();

    //         // dd($user->password);
    //         if (!$user) {
    //         //    return response()->json(['success'=>false, 'message' => 'Login Fail, please check email id']);
    //            return redirect()->back()->with('alert-info Login', 'Login Fail, please check email id');
    //         }
    //         if (!Hash::check($data['password'], $user->password)) {
    //         //    return response()->json(['success'=>false, 'message' => 'Login Fail, pls check password']);
    //            return redirect()->back()->with('alert-info Login-password', 'Login Fail, please check password');
    //         }
    //            //return view('admin.dashboard');
    //            return redirect('/')->with('alert-info success') ; 

    //     }
    // }
    public function authenticate(Request $request) {
        $credentials = $request->only('email', 'password');
        // dd($credentials);
        $auth = Auth::guard('admin');
        if ($auth->attempt($credentials)) {
            // return 'hello1';
            return redirect(route('dashboard'));
        }
           return redirect()->route('login')->with('alert-info Login-password', 'Login Fail, please check email or password');
     }

     public function logout() {
        Auth()->guard('admin')->logout();
        return view('admin.auth.login');
    }

}
