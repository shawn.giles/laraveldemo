<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

/**
* @SWG\Swagger(
* schemes={"http","https"},
* host=API_HOST,
* basePath=API_PATH,
* produces={"application/json","application/xml"},
* consumes={"application/x-www-form-urlencoded"},
* @SWG\Info(
* title=APP_NAME,
* version=API_VERSION,
* description="Private Key : - Hg1dhgKS1A1MT0AI5Pf5ydf7r6vlwgthyg9s",
* termsOfService="sha256",
* @SWG\Contact(
* name=APP_TEAM,
* email=SUPPORT_EMAIL
* ),
* ),
* )
*/



class BaseController extends Controller
{
    //
}
