<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;
use Illuminate\Support\Facades\Validator;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        echo "index called ";
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {   
                print_r($request->all()); exit(); 

        $users = new User;

        // $validator = Validator::make($request->all(), [
        //      'name' => 'required',
        //     'email' => 'required',
        //     'password' => 'required',
        //   ]);
      
        // if ($validator->fails()) {
        //     return response()->json([
        //         'status' => 0,
        //         'errors' => $validator->errors(),
        //     ], 400);
        // }
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'email' => 'required',
            'password' => 'required',
          ]);
      
        if ($validator->fails()) {
            return response()->json([
                'status' => 0,
                'errors' => $validator->errors(),
            ], 400);
        }

        $data = $request->all();
        //print_r($data); exit(); 

        $users->name = $data['name'];
        $users->email = $data['email'];
        $users->password = $data['password'];

        $users->save();
        return response()->json(["status" => 200, "message" => "Success"]);

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function showall()
    {
        $users = User::all();
        return response()->json(["status" => 200, "message" => "Success"]);
    }

    public function showbyid($id)
    {
        $users = User::find($id);
        if(is_null($users)){
            return response()->json(["status" => 404 ,"message" => "Record Not Found"]);
        }
        return response()->json(["status" => 200, "message" => "Success"]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function updatebyid(Request $request, $id)
    {
        $data = $request->all();
        $users = User::find($id);
        if(is_null($users)){
            return response()->json(["status" => 404 ,"message" => "Record Not Found"]);
        }
        $users->name = $data['name'];
        $users->email = $data['email'];
        $users->password = $data['password'];

        $users->save();
        return response()->json(["status" => 200, "message" => "Success"]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function deletebyid($id)
    {
        $users = User::find($id);
        if(is_null($users)){
            return response()->json(['status' => 404 ,'message' => 'Record Not Found ']);
        }
        $users->delete();
        return response()->json(["status" => 200, "message" => "Success"]);
    }
}
