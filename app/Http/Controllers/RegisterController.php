<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Register;

class RegisterController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.auth.register');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {   
        //echo"store called in redistration"; exit;
        $repetedemail = trim(strtolower(request('email')));
		if(Register::where('email', '=', $repetedemail)->count()){
			return redirect()->back()->with('alert-info', 'Email Is Already Exists.');
		}
        else{   
            if ($request->isMethod('post')) {
                $data = $request->all();
                    if($data['password'] == $data['cpassword']){
                        $category = new Register;
                        $category->name   = $data['full_name'];
                        $category->email = $data['email'];
                        $category->password = bcrypt($data['password']);
                        $category->save();

                        return redirect('/');
                    }else{
                        return redirect()->back()->with('alert-password-info', 'Password & ConformPassword Not Match.');
                    }

            }
        }   
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
