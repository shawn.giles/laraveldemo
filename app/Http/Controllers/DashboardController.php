<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Category;

class DashboardController extends Controller
{
    public function index()
    {   
        // $category = new Category;

        // $category->c_name = "pan";
        // $category->c_images = "catgegory.php";
        // $category->c_orders = 20;
        // $category->c_status = 1;
        // $category->c_added_date = 111502;
        // $category->c_updated_date = 145151;

        // $category->save();

        return view('admin.dashboard');
    }
}
