@extends('admin.layouts.regi')
@section('title' , 'Register a new  membership')
@section('content')
    
    <form role="form" id="form" action="{{ route('registerData') }}" method="post">
    @csrf
        <div class="input-group mb-3">
        <input type="text" name="full_name" class="form-control" placeholder="Full name" required="">
        <div class="input-group-append">
            <div class="input-group-text">
            <span class="fas fa-user"></span>
            </div>
        </div>
        </div>
        <div class="input-group mb-3">
        <input type="email" name="email" class="form-control" placeholder="Email" required="">
        <div class="input-group-append">
            <div class="input-group-text">
            <span class="fas fa-envelope"></span>
            </div>
        </div>
        </div>
        <div class="input-group mb-3">
        <input type="password" name="password" class="form-control" placeholder="Password" required="">   
        <div class="input-group-append">
            <div class="input-group-text">
            <span class="fas fa-lock"></span>
            </div>
        </div>
        </div>
        <div class="input-group mb-3">
          <input type="password" class="form-control" name="cpassword" placeholder="Retype password" required="">
          <div class="input-group-append">
            <div class="input-group-text">
              <span class="fas fa-lock"></span>
            </div>
          </div>
        </div> 
        <div class="row">
        <div class="col-8">
            <div class="icheck-primary">
            <input type="checkbox" id="agreeTerms" name="terms" value="agree">
            <label for="agreeTerms">
            I agree to the <a href="#">terms</a>
            </label>
            </div>
        </div>
        <!-- /.col -->
        <div class="col-4">
            <button type="submit" class="btn btn-primary btn-block" name="submit">Register</button>
        </div>
        <!-- /.col -->
        </div>
        <a href="{{ route('login') }}" class="text-center">I already have a membership</a>
    </form>
@endsection