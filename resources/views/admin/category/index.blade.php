@extends('admin.layouts.app')
@section('title', 'Category')
@section('content')
@if(session()->has('success'))
    <div class="alert alert-success">
        {{ session()->get('success') }}
    </div>
@endif
@if(session()->has('success-delete'))
    <div class="alert alert-success">
        {{ session()->get('success-delete') }}
    </div>
@endif
<div class="content-header">
<div class="row">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
					<h3 class="m-0 text-dark">Category List</h3>
				</div>
                <div class="col-sm-6">
                    <ol class="float-sm-right">
                        <a href="{{ route('catagoryadd') }}"><button type="button" class="btn btn-block btn-default float-sm-left" onclick="addCategory()">Add Category</button></a>
                    </ol>
                </div>
            </div>
      </div><!-- /.container-fluid -->
</div>
</div>
<div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <table id="example1" class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Name</th>
                                    <th>Image</th>
                                    <th>Order</th>
                                    <th>Added_Date</th>
                                    <th>Modifid_Date</th>
                                    <th>Status</th>
                                    <th>Edit</th>
                                    <th>Delete</th>
                                </tr>
                            </thead>
                            <tbody> 
                            @php
                                $number = 1; ;
                            @endphp
                                @foreach($category as $row)
                                <tr>
                                    <input type="hidden" class="serdelete_val_id" value="{{ $row['id'] }}">
                                    <td>{{ $number }}</td>
                                    <td>{{ $row['c_name'] }}</td>
                                    <!-- <td>{{ $row['c_images'] }}</td> --><td>
                                    <img src="{{ url('storage/app/public/category_images') }}/{{ $row['c_images'] }}" alt="Not Any Image Found." width="100px" height="100px" >
                                    </td>
                                    <td>{{ $row['c_orders'] }}</td>
                                    <td>{{ date('Y-m-d', $row['c_added_date']) }}</td>
                                    <td>{{ date('Y-m-d', $row['c_updated_date']) }}</td>
                                    <td>{{ isset($row['c_status']) && $row['c_status'] ==  1  ? "Active" : 'In-Active'}}</td>
                                    <td>    
                                        <a href="{{url('category/catagoryEdit')}}/{{ $row['id'] }}"  class="btn btn-warning">Edit</a>
                                    </td>
                                    <td><a href="javascript:;" onclick="removeCategory({{ $row['id'] }})" role="button" class="btn btn-danger">Delete</a></td>
                                    </td>
                                    @php
                                        $number++;
                                    @endphp
                                </tr>
                                
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                  <!-- /.card-body -->
                </div>
            <!-- /.card -->
            </div>
        </div>
        <!-- /.row -->
</div>
<!-- Modal -->
<div class="modal fade" id="removeCategoryModal" role="dialog">
    <div class="modal-dialog ">
      <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                    <h4 class="modal-title">Delete Category</h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
                    <p>Please Confirm That You Want To  Delete Selected Catagory</p>
            </div>
            <div class="modal-footer">
                <div class="form-group"> 
                    <button type="button" class="btn btn-default float-left" data-dismiss="modal">Close</button>
                </div>
                <div class="form-group"> 
                        <a href="" class="btn btn-danger float-left" id="removeAcceptButton">Accept</a>
                </div>
            </div>
        </div>
      
    </div>
</div>

@endsection

<link rel="stylesheet" href="{{ adminAssets('plugins/datatables-bs4/css/dataTables.bootstrap4.min.css') }}">
<link rel="stylesheet" href="{{ adminAssets('plugins/datatables-responsive/css/responsive.bootstrap4.min.css') }}">

<script src="{{ adminAssets('plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ adminAssets('plugins/datatables-bs4/js/dataTables.bootstrap4.min.js') }}"></script>
<script src="{{ adminAssets('plugins/datatables-responsive/js/dataTables.responsive.min.js') }}"></script>
<script src="{{ adminAssets('plugins/datatables-responsive/js/responsive.bootstrap4.min.js') }}"></script>

<script>
    $(function () {
        $("#example1").DataTable({
            "responsive": true,
            "autoWidth": false,
        });
    });
</script>

