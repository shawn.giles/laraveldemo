@extends('admin.layouts.app')
@section('label', 'Add Category')
@section('content')
@if($errors->any())
@foreach ($errors->all() as $error)
<div class="alert alert-dismissible fade show" style="background-color: #e69e9e;" role="alert">
    {{$error}}
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
</div>
@endforeach
@endif
<section class="content-header">
            <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                @if (isset($editcategory)) 
                  <h1> Edit Category </h1>
                @else
                  <h1> Add Category </h1>
				@endif
                </div>
            </div>
            </div>
        </section>
<section class="content">
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-12">
				<div class="card card-primary">
				@if(Session::has('alert-info'))
					<p class="alert alert-danger">{{ Session::get('alert-info') }}</p>
				@endif
					<form role="form" id="form" action="{{ route('catagoryData')}}" data-parsley-validate="" method="post" enctype="multipart/form-data">
				
                    @csrf
					<input type="hidden" name="id" value="{{ $editcategory->id ?? ''}}">

						<div class="card-body">
							<div class="form-group">
								<label>Name {!! _star()!!} </label>
									<input type="text" class="form-control" data-parsley-errors-container="#message_container_l_body" name="c_name" value="{{ $editcategory->c_name ?? ''}}" placeholder="Enter Name" required="">
									<span id="message_container_l_body" ></span>
							</div>
							
							@if(isset($editcategory))
							<div class="form-group">
								<label>Image {!! _star()!!}</label>
									<div class="input-group">
										<div class="custom-file">
										<input type="hidden" name="old_imgs" value="{{ $editcategory->c_images ?? ''}}">

											<input type="file" class="form-control" name="c_images" accept="image/jpg, image/jpeg, image/png">
											<!-- <label class="custom-file-label">Choose file</label>  -->
										</div>
									</div>
									@if(isset($editcategory))
											@php
												$image = url('storage/app/public/category_images')."/".$editcategory->c_images ;
											@endphp
												<img src="{{ $image }}" alt="Not Any Image Found." width="100px" height="100px">
									@endif
									<h6 class="serif">Allowed file extansion is jpg, jpeg & png</h6>
									<span id="message_container_p_body"></span>
							</div>
							@else

							<div class="form-group">
								<label>Image {!! _star()!!}</label>
									<div class="input-group">
										<div class="custom-file">
											<input type="file" class="form-control" data-parsley-errors-container="#message_container_p_body" name="c_images" required="" accept="image/jpg, image/jpeg, image/png">
											<!-- <label class="custom-file-label">Choose file</label>  -->
										</div>
										
									</div>
									<h6 class="serif">Allowed file extansion is jpg, jpeg & png</h6>
									<span id="message_container_p_body"></span>
							</div>
							
							@endif		

							<div class="form-group">
								<label>Order {!! _star()!!}  </label>
									<input type="number" class="form-control" name="c_orders" value="{{ $editcategory->c_orders ?? ''}}" placeholder="Enter Order Number" required="">
							</div>
							<div class="form-group">
								<label>Status {!! _star()!!}</label>
									<select class="form-control" name="c_status" required="">
									  	<option  @if(isset($editcategory->c_status ) && $editcategory->c_status == '1')) selected @endif value='1'>ACTIVE</option>
									  	<option @if(isset($editcategory->c_status ) && $editcategory->c_status == '0')) selected @endif value='0'>IN-ACTIVE</option>
									</select>
							</div>
						</div>
						@if (isset($editcategory)) 
							<div class="card-footer">
								<button type="submit" class="btn btn-primary">Save</button>
									<a href="{{ route('catagorylist') }}"><input type="button" class="btn btn-primary" name="cancel" value="Cancel"/></a>         
							</div>
						@else
							<div class="card-footer">
								<button type="submit" class="btn btn-primary">Submit</button>
									<a href="{{ route('catagorylist') }}"><input type="button" class="btn btn-primary" name="cancel" value="Cancel"/></a>         
							</div>
						@endif
						
					</form>
				</div>
			<div>                                                                                                             
		</div>
	</div>
</section>     
@endsection

