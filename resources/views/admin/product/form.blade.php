@extends('admin.layouts.app')

@section('label', 'Add Product')
@section('content')
@if($errors->any())
@foreach ($errors->all() as $error)
<div class="alert alert-dismissible fade show" style="background-color: #e69e9e;" role="alert">
    {{$error}}
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
</div>
@endforeach
@endif
<section class="content-header">
	<div class="container-fluid">
	<div class="row mb-2">
		<div class="col-sm-6">
		@if (isset($editProduct)) 
		  <h1> Edit Product </h1>
		@else
		  <h1> Add Product </h1>
		@endif
		</div>
	</div>
	</div>
</section>
<section class="content">
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-12">
				<div class="card card-primary">
					<form role="form" id="form" action=" {{ route('productData') }}" data-parsley-validate="" method="post" enctype="multipart/form-data">
                    @csrf
					
					<input type="hidden" name="id" value="{{ $editProduct[0]['id'] ?? ''}}">
					<input type="hidden" name="old_PimgNAMES" value="{{ $editProduct[0]['PimgNAMES'] ?? ''}}">

						<div class="card-body">
						<div class="form-group">
								<label>Images {!! _star()!!}</label>
									<div class="input-group">
										<div class="custom-file">

											<input type="file" class="form-control" data-parsley-errors-container="#message_container_i_body" name="p_images[]" value="{{ $editProduct->p_images ?? ''}}" multiple {{ isset($id) ? "": "required=''" }}  accept="image/jpg, image/jpeg, image/png">
											<!-- <label class="custom-file-label">Choose file</label>  -->
										</div>
									</div>
									<h6 class="serif">Allowed file extansion is jpg, jpeg & png</h6>
									@if(isset($editProduct))
										@php 
											$imgs = explode(",", $editProduct[0]['PimgNAMES'] );
										@endphp
										@foreach( $imgs as $imagess)
											@php
												$image = url('storage/app/public')."/".$imagess ;
											@endphp
												<img src="{{ $image }}" alt="Not Any Image Found." width="100px" height="100px">
										@endforeach
									@endif
									<span id="message_container_i_body"></span>
							</div>	
							
							<div class="form-group">
								<label>Name {!! _star()!!} </label>
									<input type="text" class="form-control" data-parsley-errors-container="#message_container_n_body" name="p_name" value="{{ $editProduct[0]['p_name'] ?? ''}}" placeholder="Enter Name" required="">
									<span id="message_container_n_body" ></span>
							</div>
							@if(isset($editProduct))
							@php 
								$catids = explode(",", $editProduct[0]['catIDS'] );
								
							@endphp
							@endif
							<div class="form-group">
								<label>Category {!! _star()!!} </label>
									<select class="select2" name="category[]"  multiple data-parsley-errors-container="#message_container_oy_body" required="" data-placeholder="Select a Category" style="width: 100%;">
										
										@if(!empty($categorys))
											
											@foreach($categorys as $category)	
													<option value="{{ $category['id'] }}" > {{ $category['c_name'] }} </option>
											@endforeach
										@endif
										@if(!empty($editProduct))
											
											@foreach($categorys as $category)	
													<option value="{{ $category['id'] }}"  @if(in_array($category['id'] , $catids)) selected @endif > {{ $category['c_name'] }} </option>
											@endforeach
										@endif
									</select>
	                            <span id="message_container_oy_body"></span>
							</div>
							<div class="form-group">
								<label>Price {!! _star()!!} </label>
									<input type="number" class="form-control" data-parsley-errors-container="#message_container_p_body" name="p_price" value="{{ $editProduct[0]['p_price'] ?? ''}}" placeholder="Enter Name" required="">
									<span id="message_container_p_body" ></span>
							</div>
							<div class="form-group">
								<label>Sale Price {!! _star()!!} </label>
									<input type="number" class="form-control" data-parsley-errors-container="#message_container_s_body" name="p_sale_price" value="{{ $editProduct[0]['p_sale_price'] ?? ''}}" placeholder="Enter Name" required="">
									<span id="message_container_s_body" ></span>
							</div>
							<div class="form-group">
								<label>Quantity {!! _star()!!} </label>
									<input type="number" class="form-control" data-parsley-errors-container="#message_container_q_body" name="p_quantity" value="{{ $editProduct[0]['p_quantity'] ?? ''}}" placeholder="Enter Name" required="">
									<span id="message_container_q_body" ></span>
							</div>
														  	
							<div class="form-group">
								<label>Order {!! _star()!!}  </label>
									<input type="number" class="form-control" data-parsley-errors-container="#message_container_o_body" name="p_orders" value="{{ $editProduct[0]['p_orders'] ?? ''}}" placeholder="Enter Order Number" required="">
									<span id="message_container_o_body" ></span>
							</div>
							<div class="form-group">
								<label>Status {!! _star()!!}</label>
									<select class="form-control" name="p_status" required="">
									  	<option @if(isset($editProduct[0]['p_status'] ) && $editProduct[0]['p_status'] == '1') selected @endif value='1'>ACTIVE</option>
									  	<option @if(isset($editProduct[0]['p_status'] ) && $editProduct[0]['p_status'] == '0') selected @endif value='0'>IN-ACTIVE</option>
									</select>
							</div>
						</div>
						
						@if (isset($editProduct)) 
							<div class="card-footer">
								<button type="submit" class="btn btn-primary">Save</button>
									<a href="{{ route('productlist') }}"><input type="button" class="btn btn-primary" name="cancel" value="Cancel"/></a>         
							</div>
						@else
							<div class="card-footer">
								<button type="submit" class="btn btn-primary">Submit</button>
									<a href="{{ route('productlist') }}"><input type="button" class="btn btn-primary" name="cancel" value="Cancel"/></a>         
							</div>
						@endif
					</form>
				</div>
			</div>
		</div>
	</div>
</section>     
@endsection

