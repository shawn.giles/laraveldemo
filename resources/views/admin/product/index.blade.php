@extends('admin.layouts.app')
@section('title', 'Product')
@section('content')
@if(session()->has('success'))
    <div class="alert alert-success">
        {{ session()->get('success') }}
    </div>
@endif
@if(session()->has('success-delete'))
    <div class="alert alert-success">
        {{ session()->get('success-delete') }}
    </div>
@endif
<div class="content-header">
<div class="row">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
					<h3 class="m-0 text-dark">Product List</h3>
				</div>
                <div class="col-sm-6">
                    <ol class="float-sm-right">
                        <a href="{{ route('productadd') }}"><button type="button" class="btn btn-block btn-default float-sm-left">Add Product</button></a>
                    </ol>
                </div>
            </div>
      </div><!-- /.container-fluid -->
</div>
</div>
<div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <table id="example1" class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Product_Code</th>
                                    <th>Image</th>
                                    <th>Name</th>
                                    <th>Category_Names</th>
                                    <th>Product_Price</th>
                                    <th>Sale_Product_Price</th>
                                    <th>Product_Quantity</th>
                                    <th>Product_Orders</th>
                                    <th>Added_Date</th>
                                    <th>Modifid_Date</th>
                                    <th>Status</th>
                                    <th>Edit</th>
                                    <th>Delete</th>
                                </tr>
                            </thead>
                            <tbody>
                            @php
                                $number = 1; 
                            @endphp
                            @if(!empty($product) && $product->count())                            
                                @foreach($product as $row)
                                            
                                <tr>
                                    <td>{{ $number }}</td>
                                    <td>{{ isset($row->product_code) ? $row->product_code : ''  }}</td>
                                    <td>
                                    <img src="{{ url('storage/app/public') }}/{{ $row->Pimgs }}" alt="Not Any Image Found." width="100px" height="100px" >
                                    </td>
                                    <td>{{ isset($row->p_name) ? $row->p_name : '' }}</td>
                                    <td>{{ isset($row->CatNAMES) ? $row->CatNAMES : '' }}</td>
                                    <td>{{ isset($row->p_price) ? $row->p_price : ''}}</td>
                                    <td>{{ isset($row->p_sale_price) ? $row->p_sale_price : ''}}</td>
                                    <td>{{ isset($row->p_quantity) ? $row->p_quantity : ''}}</td>
                                    <td>{{ isset($row->p_orders) ? $row->p_orders : ''}}</td>
                                    <td>{{ date('Y-m-d', $row->p_added_date) }}</td>
                                    <td>{{ date('Y-m-d', $row->p_updated_date) }}</td>
                                    <td>{{ isset($row->p_status) && $row->p_status ==  1  ? "Active" : 'In-Active'}}</td>
                                    <td>    
                                        <a href="{{url('product/productEdit')}}/{{ $row->id }}"  class="btn btn-warning">Edit</a>
                                    </td>
                                    <td><a href="javascript:;" onclick="removeProduct({{ $row->id }})" role="button" class="btn btn-danger">Delete</a></td>
                                    </td>
                                    @php
                                        $number++;
                                    @endphp
                                </tr>
                                @endforeach
                            @endif    
                            </tbody>
                        </table>
                    </div>
                  <!-- /.card-body -->
                </div>
            <!-- /.card -->
            </div>
        </div>
        <!-- /.row -->
</div>
<!-- Modal -->
<div class="modal fade" id="removeProductModal" role="dialog">
    <div class="modal-dialog ">
      <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                    <h4 class="modal-title">Delete Category</h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
                    <p>Please Confirm That You Want To  Delete Selected Catagory</p>
            </div>
            <div class="modal-footer">
                <div class="form-group"> 
                    <button type="button" class="btn btn-default float-left" data-dismiss="modal">Close</button>
                </div>
                <div class="form-group"> 
                        <a href="" class="btn btn-danger float-left" id="removeProductAcceptButton">Accept</a>
                </div>
            </div>
        </div>
      
    </div>
</div>
@endsection

<link rel="stylesheet" href="{{ adminAssets('plugins/datatables-bs4/css/dataTables.bootstrap4.min.css') }}">
<link rel="stylesheet" href="{{ adminAssets('plugins/datatables-responsive/css/responsive.bootstrap4.min.css') }}">

<script src="{{ adminAssets('plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ adminAssets('plugins/datatables-bs4/js/dataTables.bootstrap4.min.js') }}"></script>
<script src="{{ adminAssets('plugins/datatables-responsive/js/dataTables.responsive.min.js') }}"></script>
<script src="{{ adminAssets('plugins/datatables-responsive/js/responsive.bootstrap4.min.js') }}"></script>

<script>
  $(function () {
    $("#example1").DataTable({
        "responsive": true,
        "autoWidth": false,
    });
  });
</script>