<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>AdminLTE 3 | @yield('title') </title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="{{adminAssets('plugins/fontawesome-free/css/all.min.css')}}">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Select2 -->
  <link rel="stylesheet" href="{{adminAssets('plugins/select2/css/select2.min.css')}}">
  <link rel="stylesheet" href="{{adminAssets('plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css')}}">
  <!-- Bootstrap4 Duallistbox -->
  <link rel="stylesheet" href="{{adminAssets('plugins/bootstrap4-duallistbox/bootstrap-duallistbox.min.css')}}">
  <!-- Tempusdominus Bbootstrap 4 -->
  <link rel="stylesheet" href="{{adminAssets('plugins/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.min.css')}}">
  <!-- iCheck -->
  <link rel="stylesheet" href="{{adminAssets('plugins/icheck-bootstrap/icheck-bootstrap.min.css')}}">
  <!-- JQVMap -->
  <link rel="stylesheet" href="{{adminAssets('plugins/jqvmap/jqvmap.min.css')}}">
  <!-- Theme style -->
  <link rel="stylesheet" href="{{adminAssets('dist/css/adminlte.min.css')}}">
  <link rel="stylesheet" href="{{adminAssets('dist/css/adminlte.css')}}">
  <link rel="stylesheet" href="{{adminAssets('dist/css/custom.css')}}">
  <!-- overlayScrollbars -->
  <link rel="stylesheet" href="{{adminAssets('plugins/overlayScrollbars/css/OverlayScrollbars.min.css')}}">
  <!-- Daterange picker -->
  <link rel="stylesheet" href="{{adminAssets('plugins/daterangepicker/daterangepicker.css')}}">
  <!-- summernote -->
  <link rel="stylesheet" href="{{adminAssets('plugins/summernote/summernote-bs4.css')}}">
  <!-- Google Font: Source Sans Pro -->
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
  <link rel="stylesheet" href="{{adminAssets('plugins/datatables-bs4/css/dataTables.bootstrap4.min.css')}}">
  <link rel="stylesheet" href="{{adminAssets('plugins/datatables-responsive/css/responsive.bootstrap4.min.css')}}">


</head>
<body class="hold-transition sidebar-mini layout-fixed">
<div class="wrapper">

  <!-- Navbar -->
  @include('admin.partials.nav')
  <!-- /.navbar -->

  <!-- Main Sidebar Container -->
  @include('admin.partials.aside')

  <!-- Content Wrapper. Contains page content -->
	<div class="content-wrapper">
		<!-- Content Header (Page header) -->
		
		<!-- /.content-header -->

		<!-- Main content -->
		<section class="content">
			<div class="container-fluid">
				@yield('content')
			</div><!-- /.container-fluid -->
		</section>
		<!-- /.content -->
	</div>
  	<!-- /.content-wrapper -->
	<footer class="main-footer">
		<strong>Copyright &copy; 2014-2019 <a href="http://adminlte.io">AdminLTE.io</a>.</strong>
		All rights reserved.
		<div class="float-right d-none d-sm-inline-block">
			<b>Version</b> 3.0.5
		</div>
	</footer>

  	<!-- Control Sidebar -->
	<aside class="control-sidebar control-sidebar-dark">
		<!-- Control sidebar content goes here -->
	</aside>
  	<!-- /.control-sidebar -->
</div>
<!-- ./wrapper -->

<!-- jQuery -->
<script src="{{adminAssets('plugins/jquery/jquery.min.js')}}"></script>
<!-- jQuery UI 1.11.4 -->
<script src="{{adminAssets('plugins/jquery-ui/jquery-ui.min.js')}}"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
  $.widget.bridge('uibutton', $.ui.button)
</script>
<!-- Bootstrap 4 -->
<script src="{{adminAssets('plugins/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
<!-- ChartJS -->
<script src="{{adminAssets('plugins/chart.js/Chart.min.js')}}"></script>
<!-- Sparkline -->
<script src="{{adminAssets('plugins/sparklines/sparkline.js')}}"></script>
<!-- JQVMap -->
<script src="{{adminAssets('plugins/jqvmap/jquery.vmap.min.js')}}"></script>
<script src="{{adminAssets('plugins/jqvmap/maps/jquery.vmap.usa.js')}}"></script>
<!-- jQuery Knob Chart -->
<script src="{{adminAssets('plugins/jquery-knob/jquery.knob.min.js')}}"></script>
<!-- daterangepicker -->
<script src="{{adminAssets('plugins/moment/moment.min.js')}}"></script>
<script src="{{adminAssets('plugins/daterangepicker/daterangepicker.js')}}"></script>
<!-- Tempusdominus Bootstrap 4 -->
<script src="{{adminAssets('plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js')}}"></script>
<!-- Summernote -->
<script src="{{adminAssets('plugins/summernote/summernote-bs4.min.js')}}"></script>
<!-- overlayScrollbars -->
<script src="{{adminAssets('plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js')}}"></script>
<!-- AdminLTE App -->
<script src="{{adminAssets('dist/js/adminlte.js')}}"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<script src="{{adminAssets('dist/js/pages/dashboard.js')}}"></script>
<!-- AdminLTE for demo purposes -->
<script src="{{adminAssets('dist/js/demo.js')}}"></script>
<script src="{{adminAssets('plugins/parsley/parsley.js')}}"></script>
<script src="{{adminAssets('dist/js/adminlte.js')}}"></script>
<script src="{{adminAssets('plugins/select2/js/select2.full.min.js')}}"></script>
<!-- Bootstrap4 Duallistbox -->
<script src="{{adminAssets('plugins/bootstrap4-duallistbox/jquery.bootstrap-duallistbox.min.js')}}"></script>

<script src="{{adminAssets('plugins/datatables/jquery.dataTables.min.js')}}"></script>
<script src="{{adminAssets('plugins/datatables-bs4/js/dataTables.bootstrap4.min.js')}}"></script>
<script src="{{adminAssets('plugins/datatables-responsive/js/dataTables.responsive.min.js')}}"></script>
<script src="{{adminAssets('plugins/datatables-responsive/js/responsive.bootstrap4.min.js')}}"></script>

@stack('js')
@stack('jsfun')
</body>
</html>
<script>
  $(function () {
    $("#example1").DataTable({
        "responsive": true,
        "autoWidth": false,
    });
  });
  $(function () {
    //Initialize Select2 Elements
    $('.select2').select2();
    //Initialize Select2 Elements
    $('.select2bs4').select2({
        theme: 'bootstrap4'
    });

  })
</script>

<script>
    // $(document).ready(function () {

    //     $('.servicedeletebtn').click(function (e) {
    //         e.preventDefault();
    //         //alert("hello are u sure ?? ");

    //         var delete_id = $(this).closest("tr").find('.serdelete_val_id').val();
    //         alert(delete_id);
    //     });

    // });  
    function removeCategory(deleteid){
      //alert("hello");
        $('#removeCategoryModal').modal('show');
        $('#removeAcceptButton').attr('href',"{{url('/category/catagoryDelete/')}}/"+deleteid);
    }
    function removeProduct(deleteid){
      //alert("hello");
        $('#removeProductModal').modal('show');
        $('#removeProductAcceptButton').attr('href',"{{url('/product/productDelete/')}}/"+deleteid);
    }  
</script>
