<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>AdminLTE 3 | Registration Page</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <!-- Font Awesome -->
  <link rel="stylesheet" href="{{adminAssets('plugins/fontawesome-free/css/all.min.css')}}">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css')}}">
  <!-- icheck bootstrap -->
  <link rel="stylesheet" href="{{adminAssets('plugins/icheck-bootstrap/icheck-bootstrap.min.css')}}">
  <!-- Theme style -->
  <link rel="stylesheet" href="{{adminAssets('dist/css/adminlte.min.css')}}">
  <!-- Google Font: Source Sans Pro -->
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
</head>

<body class="hold-transition register-page">
<div class="register-box">
    @if(Session::has('alert-info'))
        <p class="alert alert-info">{{ Session::get('alert-info') }}</p>
    @endif
    @if(Session::has('alert-password-info'))
        <p class="alert alert-info">{{ Session::get('alert-password-info') }}</p>
    @endif
    @if(Session::has('alert-info Login'))
        <p class="alert alert-info">{{ Session::get('alert-info Login') }}</p>
    @endif
    @if(Session::has('alert-info Login-password'))
        <p class="alert alert-danger">{{ Session::get('alert-info Login-password') }}</p>
    @endif
        <div class="register-logo">
            <a href="../../index2.html"><b>Admin</b>LTE</a>
        </div>

        
        <div class="card">
            <div class="card-body register-card-body">
            <p class="login-box-msg"> @yield('title')</p>
                @yield('content')
            </div>
            <!-- /.form-box -->
        </div><!-- /.card -->
    </div>

<!-- jQuery -->
<script src="{{adminAssets('plugins/jquery/jquery.min.js')}}"></script>
<!-- Bootstrap 4 -->
<script src="{{adminAssets('plugins/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
<!-- AdminLTE App -->
<script src="{{adminAssets('dist/js/adminlte.min.js')}}"></script>
</body>
</html>